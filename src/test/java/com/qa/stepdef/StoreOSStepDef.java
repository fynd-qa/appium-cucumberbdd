package com.qa.stepdef;

import io.cucumber.java.en.Given;
import com.qa.pages.StoreOSPage;
import com.qa.pages.StoreOSProducts;
import io.cucumber.java.en.Then;

public class StoreOSStepDef {

    StoreOSPage Page = new StoreOSPage();
    StoreOSProducts ProductPage = new StoreOSProducts();
    @Given("I login in to Fynd Store OS using {string} and {string}")
    public void StoreOSLogin(String username, String password) {

        Page.login(username, password);
    }

    @Then("I am selecting the business account with sales channel and selling location using {string} and {string} and {string}")
    public void SelectStore(String businessAccount, String salesChannel, String selllocation) throws InterruptedException {
        Page.selectStore(businessAccount, salesChannel, selllocation);
    }


    @Then("Place Order for Product={string} OrderType={string} and PaymentType={string}")
    public void placeOrder(String productName,String OrderType,String PaymentType) throws InterruptedException {
        ProductPage.placeOrder(productName,OrderType,PaymentType);
    }

}
