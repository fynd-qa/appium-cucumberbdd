package com.qa.pages;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

import java.util.Random;

public class StoreOSCustomer extends BasePage{

    //Customer Details
    @AndroidFindBy(xpath = "//*[@resource-id=\"btn_addressHolder\"]")
    private WebElement addCustomer;

    @AndroidFindBy(xpath = "//android.view.View[2]/android.view.View/android.widget.EditText")
    private WebElement enterCustomerDetails;

    @AndroidFindBy(xpath = "//android.view.View[2]/android.view.View/android.view.View[2]")
    private WebElement CutomerSelection;

    @AndroidFindBy(xpath = "//android.view.View[3]/android.widget.Button")
    private WebElement proceedButton;


    public void addCustomer(String type){
        if (type == "Existing"){
            click(addCustomer);
            sendKeys(enterCustomerDetails,"8209208341");
            click(CutomerSelection);
            click(proceedButton);
        }else{
            String new_n0 = StoreOSCustomer.generateRandomMobileNumber();

        }

    }
    public static String generateRandomMobileNumber() {
        Random random = new Random();

        // Assuming a standard 10-digit mobile number format
        StringBuilder mobileNumber = new StringBuilder("9"); // Start with a digit that mobile numbers typically start with

        for (int i = 0; i < 9; i++) {
            mobileNumber.append(random.nextInt(10)); // Append random digits
        }

        return mobileNumber.toString();
    }




}
