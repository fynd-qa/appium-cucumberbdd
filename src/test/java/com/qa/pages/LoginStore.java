package com.qa.pages;

import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

public class LoginStore extends BasePage {


    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.compose.ui.platform.ComposeView/android.view.View/android.view.View/android.widget.ScrollView/android.widget.EditText")
    private WebElement phoneNumberText;

    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.compose.ui.platform.ComposeView/android.view.View/android.view.View/android.widget.ScrollView/android.view.View/android.widget.Button")
    private WebElement continueButton;

    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.compose.ui.platform.ComposeView/android.view.View/android.view.View/android.widget.ScrollView/android.widget.TextView[7]")
    private WebElement usePasswordButton;

    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.compose.ui.platform.ComposeView/android.view.View/android.view.View/android.widget.ScrollView/android.widget.EditText")
    private WebElement passwordInputText;

    public void login(String username,String password){
        clear(phoneNumberText);
        sendKeys(phoneNumberText,username);
       // waitForElemToBeVisible(continueButton);
        click(continueButton);
       // waitForElemToBeVisible(usePasswordButton);
        click(usePasswordButton);
      //  waitForElemToBeVisible(passwordInputText);
        sendKeys(passwordInputText,password);
    //    waitForElemToBeVisible(continueButton);
        click(continueButton);
     //   reload();
    }
}
