package com.qa.pages;

import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;
import static org.hamcrest.MatcherAssert.assertThat;
import com.qa.pages.StoreOSCustomer;

public class StoreOSProducts extends  BasePage {


    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.compose.ui.platform.ComposeView/android.view.View/android.view.View/android.view.View[1]/android.view.View/android.view.View/android.view.View[2]/android.view.View")
    private WebElement productsTab;

    @AndroidFindBy(xpath = "//android.view.View[2]/android.widget.EditText")
    private WebElement searchField;

    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.compose.ui.platform.ComposeView/android.view.View/android.view.View/android.view.View[6]/android.view.View/android.widget.TextView[2]")
    private WebElement itemText;

    @AndroidFindBy(xpath = "//*[@resource-id=\"item_search_item\"]")
    private WebElement searchedProduct;

    @AndroidFindBy(xpath = "//android.view.View[2]/android.widget.Button")
    private WebElement addToCartButton;

    @AndroidFindBy(xpath = "//*[@resource-id=\"btn_checkout\"]")
    private WebElement checkoutButton;

    @AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.compose.ui.platform.ComposeView/android.view.View/android.view.View/android.widget.EditText/android.view.View[3]")
    private WebElement searchClearBtn;

    //Payment Methods
    // Cash
    @AndroidFindBy(xpath = "//*[@resource-id=\"btn_Cash\"]")
    private WebElement cashButton;


    @AndroidFindBy(xpath = "//*[@resource-id=\" btn_Cash on Delivery\"]")
    private WebElement codButton;

    @AndroidFindBy(xpath = "//android.view.View/android.widget.EditText")
    private WebElement cashText;


    @AndroidFindBy(xpath = "//*[@resource-id=\"btn_continue\"]")
    private WebElement continueButton;

    @AndroidFindBy(xpath = "//android.view.View[1]/android.widget.TextView[3]")
    private WebElement orderID;

    @AndroidFindBy(xpath = "//*[@resource-id=\"btn_deliveryType\"][2]")
    private WebElement moveToDelivery;

    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/t2.i1/android.view.View/android.view.View/android.view.View[3]/android.view.View/android.view.View[2]/android.view.View")
    private WebElement AllStores;

    //Customer Details
    @AndroidFindBy(xpath = "//*[@resource-id=\"btn_addressHolder\"]")
    private WebElement addCustomer;

    @AndroidFindBy(xpath = "//android.view.View[2]/android.view.View/android.widget.EditText")
    private WebElement enterCustomerDetails;

    @AndroidFindBy(xpath = "//android.view.View[2]/android.view.View/android.view.View[2]")
    private WebElement CutomerSelection;

    @AndroidFindBy(xpath = "//android.view.View[3]/android.widget.Button")
    private WebElement proceedButton;



    public void placeOrder(String product, String  orderType, String paymentType) throws InterruptedException {
      //  click(ProductNavigation);
        click(searchField);
        sendKeys(searchField,product);
        Thread.sleep(5000);
        click(searchedProduct);
        click(addToCartButton);


     //   if (orderType == "Delivery"){
            click(moveToDelivery);
           StoreOSCustomer cust = new StoreOSCustomer();
           cust.addCustomer("Existing");
       // }

        click(checkoutButton);

        if(paymentType.equals("Cash")){
            click(cashButton);
            click(cashText);
            sendKeys(cashText,"11000");
        } else if (paymentType.equals("COD")) {
            click(codButton);
            click(cashText);
            sendKeys(cashText,"11000");
        }


        click(continueButton);

        waitForVisibility(orderID);
       String OrderID = getText(orderID,"Order id = ");
       System.out.println("********orderid******"+OrderID);
    }




}
