package com.qa.pages;

import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.WebElement;
import static org.hamcrest.MatcherAssert.assertThat;


public class StoreOSPage extends  BasePage {

     @AndroidFindBy (xpath = "//*[@resource-id=\"tf_email_mobile_number\"]" )
     @iOSXCUITFindBy (id = "test-BACK TO PRODUCTS")
    private WebElement phoneNumberText;

    @AndroidFindBy (xpath = "//*[@resource-id=\"btn_continue\"]" )
     private WebElement continueButton;

    @AndroidFindBy(xpath = "//*[@resource-id=\"tv_use_password\"]")
    private WebElement usePasswordButton;

    @AndroidFindBy(xpath = "//android.view.View[1]/android.widget.EditText")
    private WebElement passwordInputText;

    @AndroidFindBy(xpath = "//android.view.View[2]/android.widget.EditText")
    private WebElement StoreSearchBox;

    @AndroidFindBy(xpath = "//android.view.View[3]/android.widget.EditText")
    private WebElement StoreSearchBox2;


    @AndroidFindBy(xpath = "//*[@resource-id=\"tv_CompanyItem\"]")
    private WebElement businessAccountOption;

    @AndroidFindBy(xpath = "//*[@resource-id=\"tv_SalesChannelItem\"]")
    private WebElement storeSalesChannel;


    @AndroidFindBy(xpath = "//android.view.View[4]/android.view.View[1]")
    private WebElement Store;


    public void login(String username,String password){
        sendKeys(phoneNumberText, username, "login with username " + username);
        click(continueButton);
        click(usePasswordButton);
        sendKeys(passwordInputText, password, "login with password " + password);
        click(continueButton);
    }

    public void selectStore(String businessAccount, String salesChannel, String sellLocation ) throws InterruptedException{
        sendKeys(StoreSearchBox, businessAccount);
        Thread.sleep(2000);
        click(businessAccountOption);
        sendKeys(StoreSearchBox2, salesChannel);
        Thread.sleep(2000);
        click(storeSalesChannel);
        sendKeys(StoreSearchBox2, sellLocation);
        Thread.sleep(2000);
        click(Store);
        Thread.sleep(8000);
    }





}
