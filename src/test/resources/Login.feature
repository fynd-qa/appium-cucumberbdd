@test
Feature:Store OS Sanity Scenarios

  @StoreOS @StoreOSSanity
  Scenario Outline: Scenario :  Order="<orderType>", Checkout="<checkoutType>", Payment="<PaymentType>"
    Then Place Order for Product="<productName>" OrderType="<orderType>" and PaymentType="<PaymentType>"
    Examples:
      | productName  | orderType   | PaymentType | checkoutType | storeType |
      | Le Male         | Delivery | COD         | guest        | myStore   |
      | Le Male        | Delivery | COD         | guest        | myStore   |
      | Le Male        | Delivery | COD         | guest        | myStore   |
      | Le Male        | Delivery | COD         | guest        | myStore   |
      | Le Male        | Delivery | COD         | guest        | myStore   |
      | Le Male        | Delivery | COD         | guest        | myStore   |
      | Le Male        | Delivery | COD         | guest        | myStore   |
      #| MODISH       | PickatStore | cash        | customer        | myStore   |
       #| SATIN FLORAL | Delivery    | cash        |    customer         | myStore |
       #| SATIN FLORAL | PickatStore | cash        |    customer         | myStore |
    #  | chiragmatkar@gofynd.com | Ganesh@123 | Store OS        | Store OS     | Store OS | SATIN FLORAL | PickatStore | cash        |    customer         |